let routes = [{
  name: 'root',
  path: '/',
  pageName: 'login'
},
{
  name: 'about',
  path: '/about',
  pageName: 'about',
},
{
  name: 'home',
  path: '/home',
  pageName: 'home',
},
{
  name: 'settings',
  path: '/settings',
  pageName: 'settings',
},
{
  name: 'transaction',
  path: '/transaction',
  pageName: 'transaction',
},
{
  name: 'login',
  path: '/login',
  pageName: 'login',
},
{
  name: 'profile',
  path: '/profile',
  pageName: 'profile',
},
{
  name: 'debit',
  path: '/debit',
  pageName: 'debit',
},
{
  name: 'setpin',
  path: '/setpin',
  pageName: 'setpin',
},
{
  name: 'block',
  path: '/block',
  pageName: 'block',
},
{
  name: 'feedback',
  path: '/feedback',
  pageName: 'feedback',
}
];

exports.routes = routes;
