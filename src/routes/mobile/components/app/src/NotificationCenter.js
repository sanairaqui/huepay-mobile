"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by rohittalwar on 09/05/16.
 */
const EventEmitter = require("eventemitter3");
class NotificationCenter {
}
NotificationCenter.sharedInstance = new EventEmitter();
exports.default = NotificationCenter;
