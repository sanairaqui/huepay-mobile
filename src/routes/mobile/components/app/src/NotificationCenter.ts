/**
 * Created by rohittalwar on 09/05/16.
 */
import * as EventEmitter from "eventemitter3"

export default class NotificationCenter{

    static sharedInstance = new EventEmitter()

    // static on(event: string, fn: Function, context?: any): any{
    //
    //    return NotificationCenter.sharedInstance.on(event,fn,context)
    // }
    //
    // static emit(event:string,...args: any[]):boolean{
    //
    //     return NotificationCenter.sharedInstance.emit.apply([event].concat(args))
    // }
    //
    // static removeListener(event: string, fn?: Function, context?: any, once?: boolean): any{
    //     return NotificationCenter.sharedInstance.removeListener(event,fn,context,once)
    // }
}