"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NativeBridge_1 = require("../NativeBridge");
const NotificationCenter_1 = require("../NotificationCenter");
class BackPressHandler {
    static listen() {
        NotificationCenter_1.default.sharedInstance.on("BACK_PRESSED", () => {
            console.log("Got back press from shell");
            window.app.views.main.router.back();
        });
    }
    static isNonDissmissableDialogPresent() {
        return Dom7('.modal-preloader').length > 0;
    }
    static handleBack() {
        this.isPopupOpen() ? this.closePopups() : this.closeNonPopupViews();
    }
    static getOpenPopups() {
        let $popup = Dom7('.dismissable-popup.modal-in,.actions-modal.modal-in');
        return $popup;
    }
    ;
    static closeNonPopupViews() {
        this.isPanelOpen() ? this.closePanel() : this.popView();
    }
    ;
    static closePanel() {
        // Analytics.trackScreenView("dashboard-index")
        myApp.closePanel();
    }
    ;
    static isPanelOpen() {
        return Dom7('body').hasClass('with-panel-left-cover');
    }
    ;
    static popView() {
        this.hasHistory() ? this.goBack() : this.askShellToHandleBackPress();
    }
    ;
    static closePopups() {
        var $popups = this.getOpenPopups();
        myApp.closeModal($popups);
    }
    static isPopupOpen() {
        var $popup = this.getOpenPopups();
        return $popup.length > 0;
    }
    static hasHistory() {
        return window.mainView.history.length > 1;
    }
    static goBack() {
        window.mainView.router.back();
    }
    static askShellToHandleBackPress() {
        return NativeBridge_1.default.sendMessage("back");
    }
}
exports.default = BackPressHandler;
