"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NotificationCenter_1 = require("./NotificationCenter");
const CommonUtil_1 = require("./CommonUtil");
class NativeBridge {
    static setupNativeBridge() {
        NotificationCenter_1.default.sharedInstance.removeAllListeners("nativeResponse");
        NotificationCenter_1.default.sharedInstance.on("nativeResponse", (resp) => {
            let id = resp._id;
            let data = resp.data || {};
            id && NativeBridge.pendingPromises[id] && (data.success ? NativeBridge.pendingPromises[id].resolve(resp) : NativeBridge.pendingPromises[id].reject(resp));
            delete NativeBridge.pendingPromises[id];
        });
    }
    static pluginApiCall(params) {
        params.params = Framework7.device.ios ? CommonUtil_1.default.removeNulls(params.params) : params.params; //ios does not allow null to be stored in localNotifaction data
        let reqID = `${params.type}-${new Date().getTime()}`;
        params._id = reqID;
        let prom = new Promise((resolve, reject) => {
            NativeBridge.pendingPromises[reqID] = { resolve: resolve, reject: reject };
            NativeBridge.sendMessage("pluginApiCall", params);
        });
        return prom;
    }
    static sendMessage(name, object) {
        console.log("Sending bridge message" + ":" + name + ":" + JSON.stringify(object));
        if (Framework7.device.ios && window.webkit) {
            try {
                let handler = window.webkit.messageHandlers && window.webkit.messageHandlers[name];
                handler && (typeof handler.postMessage === "function") ? handler.postMessage(object) : null;
            }
            catch (e) {
            }
        }
        else if (window.shellBridge) {
            var param = object;
            if (object !== null && typeof object === 'object') {
                param = JSON.stringify(object);
            }
            window.shellBridge.onJavascriptEvent(name, param);
        }
    }
}
NativeBridge.plugins = {
    calendar: {
        type: "calendar",
        addSlot: "addSlot",
        removeSlot: "removeSlot"
    },
    auth: {
        type: "auth",
        login: "login",
        deviceToken: "deviceToken"
    }
};
NativeBridge.pendingPromises = {};
exports.default = NativeBridge;
//Read cordova's implementation from native to js
// https://github.com/apache/cordova-ios/blob/f75bf67438257132e739238ed579d73271b3a716/CordovaLib/Classes/Public/CDVCommandDelegateImpl.m
