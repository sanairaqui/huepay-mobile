var ModelFactory = require("pk-client").ModelFactory;  /*Javascript*/
var MODEL_PATHS = require("pk-client").MODEL_PATHS;

module.exports = class {
    onCreate() {
        this.state = {
            employee: null,
            wallet: null
        }
    }


    async refresh() {
        let mf = new ModelFactory({ api_base_url: window.app.data.base_url, token: window.app.data.token });
        this.state.employee = await mf.anyModel(MODEL_PATHS.employees).get('me');
        this.state.wallet = await mf.empWallet(this.state.employee._id).get('');
    }

}