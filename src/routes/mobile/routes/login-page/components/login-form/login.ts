import { SessionChallenge } from "pk-client";
import { Base_Url } from "../../../../../../environment"

export async function login(username: string, password: string): Promise<any> {
    let sc = new SessionChallenge(Base_Url, username, password);
    return await sc.login()
        .then(
            response => {
                if (response['token']) {
                    window.sessionStorage.setItem('auth_token', response['token']);
                    return response;
                }
                else {
                    throw response;
                }
            },
            err => {
                throw err;
            }
        );
}