"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pk_client_1 = require("pk-client");
const environment_1 = require("../../../../../../environment");
async function login(username, password) {
    let sc = new pk_client_1.SessionChallenge(environment_1.Base_Url, username, password);
    return await sc.login()
        .then(response => {
        if (response['token']) {
            window.sessionStorage.setItem('auth_token', response['token']);
            return response;
        }
        else {
            throw response;
        }
    }, err => {
        throw err;
    });
}
exports.login = login;
