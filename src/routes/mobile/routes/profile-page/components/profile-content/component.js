var ModelFactory = require("pk-client").ModelFactory;  /*Javascript*/
var MODEL_PATHS = require("pk-client").MODEL_PATHS;

module.exports = class {
    onCreate() {
        this.state = {
            employee: null,
            wallet: null,
            profile: []
        }
    }


    async refresh() {
        let mf = new ModelFactory({ api_base_url: window.app.data.base_url, token: window.app.data.token });
        this.state.employee = await mf.anyModel(MODEL_PATHS.employees).get('me');
        console.log(this.state.employee);
        this.state.wallet = await mf.empWallet(this.state.employee._id).get('');
        this.state.profile = [
            {
                name: 'Phone',
                value: this.state.employee.mobile
            },
            {
                name: 'Email',
                value: this.state.employee.email
            },
            {
                name: 'Gender',
                value: ({ 'M': 'Male', 'F': 'Female' })[this.state.employee.gender]
            },
            {
                name: 'Id Type',
                value: this.state.employee.kyc_details.idType
            },
            {
                name: 'Id Number',
                value: this.state.employee.kyc_details.idNumber
            }
        ]
    }

}