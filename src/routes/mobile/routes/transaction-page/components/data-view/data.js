let page1 = [

  {
    "date": "1st january 2018",
    "amount": 1000
  },
  {
    "date": "2nd january 2018",
    "amount": 1500
  },
  {
    "date": "10th january 2018",
    "amount": 2200
  },
  {
    "date": "15th january 2018",
    "amount": 1000
  },
  {
    "date": "21st january 2018",
    "amount": 2500
  },
  {
    "date": "25th january 2018",
    "amount": 3000
  },
  {
    "date": "26th january 2018",
    "amount": 3500
  }

];
exports.page1 = page1;
let page2 = [{
  "date": "1st february 2018",
  "amount": 1000
}, {
  "date": "2nd february 2018",
  "amount": 1500
}, {
  "date": "10th february 2018",
  "amount": 2200
}, {
  "date": "15th february 2018",
  "amount": 1000
}, {
  "date": "21st february 2018",
  "amount": 2500
}, {
  "date": "25th february 2018",
  "amount": 3000
}, {
  "date": "26th february 2018",
  "amount": 3500
}];
exports.page2 = page2;
