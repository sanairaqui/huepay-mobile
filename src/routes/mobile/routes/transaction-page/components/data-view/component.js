var ModelFactory = require("pk-client").ModelFactory;  /*Javascript*/
var MODEL_PATHS = require("pk-client").MODEL_PATHS;
const moment = require("moment-timezone");

let data = require("./data");

module.exports = class {
  onCreate() {
    this.state = {
      page1: data.page1,
      page2: data.page2,
      transactions: []

    }
  }
  async refresh() {
    let mf = new ModelFactory({ api_base_url: window.app.data.base_url, token: window.app.data.token });

    let txns = (await mf.compEmpTxns().query()).filter((t) => t.approval_status === 'approved');
    let mtxns = (await mf.merchantTxns().query()).filter((t) => t.txnStatus === 'PAYMENT_SUCCESS');
    mtxns = mtxns.map((m) => {
      return {
        initiated_ts: moment(m.txnDate, "YYYYMMDDHHmmss").tz("Asia/Kolkata").valueOf() / 1000,
        amount: m.amount,
        type: 'merchant'
      }
    })
    // console.log(mtxns);
    this.state.transactions = [...txns, ...mtxns].sort((a, b) => b.initiated_ts - a.initiated_ts);

  }

}
